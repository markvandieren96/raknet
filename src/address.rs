use super::*;
use std::net::SocketAddr;

// TODO Manage the memory cleanup & cloning
#[derive(Clone)]
pub struct Address {
	ptr: Voidptr,
}

unsafe impl Send for Address {}
unsafe impl Sync for Address {}

impl Default for Address {
	fn default() -> Address {
		Address { ptr: std::ptr::null() }
	}
}

impl Address {
	pub fn new(address: &str, port: u16) -> Address {
		Address {
			ptr: unsafe { raknet_AddressOrGUID(address.as_ptr(), port) },
		}
	}

	pub fn from_socket_addr(addr: &SocketAddr) -> Address {
		let ip = format!("{}", addr.ip());
		let port = addr.port();
		Address::new(&ip, port)
	}

	pub fn from_ptr(ptr: Voidptr) -> Address {
		Address { ptr }
	}

	pub fn ptr(&self) -> Voidptr {
		self.ptr
	}
}

impl PartialEq for Address {
	fn eq(&self, other: &Self) -> bool {
		unsafe { raknet_AddressOrGUID_Equality(self.ptr(), other.ptr()) }
	}
}
impl Eq for Address {}

impl std::hash::Hash for Address {
	fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
		let val = unsafe { raknet_AddressOrGUID_Hash(self.ptr()) };
		val.hash(state);
	}
}
