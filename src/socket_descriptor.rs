use super::*;

pub struct SocketDescriptor {
	pub ptr: Voidptr,
}

impl SocketDescriptor {
	pub fn new(port: u16, host_address: Option<&str>) -> SocketDescriptor {
		unsafe {
			SocketDescriptor {
				ptr: raknet_SocketDescriptor_Constructor(port, host_address.map(|addr| addr.as_ptr()).unwrap_or(std::ptr::null())),
			}
		}
	}
}

impl Drop for SocketDescriptor {
	fn drop(&mut self) {
		unsafe { raknet_SocketDescriptor_Destructor(self.ptr) };
	}
}
