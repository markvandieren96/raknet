mod address;
mod packet;
mod peer;
mod socket_descriptor;

pub use address::*;
pub use packet::*;
pub use peer::*;
pub use socket_descriptor::*;

pub type Voidptr = *const libc::c_void;

extern "C" {
	// RakPeerInterface
	fn raknet_RakPeerInterface_GetInstance() -> Voidptr;
	fn raknet_RakPeerInterface_DestroyInstance(peer: Voidptr);
	fn raknet_RakPeerInterface_Startup(peer: Voidptr, maxConnections: u32, socketDescriptor: Voidptr) -> u32;
	fn raknet_RakPeerInterface_Connect(peer: Voidptr, host: *const u8, port: u16) -> u32;
	fn raknet_RakPeerInterface_SetMaxIncomingConnections(peer: Voidptr, maxConnections: u16);
	fn raknet_RakPeerInterface_Receive(peer: Voidptr) -> Voidptr;
	fn raknet_RakPeerInterface_DeallocatePacket(peer: Voidptr, packet: Voidptr);
	fn raknet_RakPeerInterface_Send(peer: Voidptr, data: Voidptr, length: i32, address: Voidptr) -> u32;
	// SocketDescriptor
	fn raknet_SocketDescriptor_Constructor(port: u16, hostAddress: *const u8) -> Voidptr;
	fn raknet_SocketDescriptor_Destructor(socketDescriptor: Voidptr) -> Voidptr;
	// Packet
	fn raknet_Packet_Data(packet: Voidptr) -> *const u8;
	fn raknet_Packet_Length(packet: Voidptr) -> u32;
	fn raknet_Packet_Address(packet: Voidptr) -> Voidptr;
	// AddressOrGuid
	fn raknet_AddressOrGUID(address: *const u8, port: u16) -> Voidptr;
	fn raknet_AddressOrGUID_Equality(a: Voidptr, b: Voidptr) -> bool;
	fn raknet_AddressOrGUID_Hash(address: Voidptr) -> u32;

}
