use super::*;

pub struct Peer {
	ptr: Voidptr,
}

unsafe impl Send for Peer {}
unsafe impl Sync for Peer {}

impl Default for Peer {
	fn default() -> Peer {
		unsafe { Peer { ptr: raknet_RakPeerInterface_GetInstance() } }
	}
}

impl Peer {
	pub fn new() -> Peer {
		Peer::default()
	}

	pub fn ptr(&self) -> Voidptr {
		self.ptr
	}

	pub fn startup(&self, max_connections: u32, socket_descriptor: &SocketDescriptor) -> StartupResult {
		StartupResult::try_from(unsafe { raknet_RakPeerInterface_Startup(self.ptr, max_connections, socket_descriptor.ptr) }).unwrap()
	}

	pub fn connect(&self, host: &str, port: u16) -> ConnectionAttemptResult {
		ConnectionAttemptResult::try_from(unsafe { raknet_RakPeerInterface_Connect(self.ptr, host.as_ptr(), port) }).unwrap()
	}

	pub fn set_max_incoming_connections(&self, max_connections: u16) {
		unsafe { raknet_RakPeerInterface_SetMaxIncomingConnections(self.ptr, max_connections) };
	}

	pub fn receive(&self) -> Option<RakPacket> {
		let packet = unsafe { raknet_RakPeerInterface_Receive(self.ptr) };
		if packet.is_null() {
			None
		} else {
			Some(RakPacket::from_ptr(packet, self))
		}
	}

	pub fn send(&self, data: &[u8], address: &Address) -> Result<(), SendError> {
		let res = unsafe { raknet_RakPeerInterface_Send(self.ptr, data.as_ptr() as Voidptr, data.len() as i32, address.ptr()) };
		if res == 0 {
			Err(SendError)
		} else {
			Ok(())
		}
	}
}

#[derive(Debug)]
pub struct SendError;

impl std::fmt::Display for SendError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "Failed to send message")
	}
}

impl std::error::Error for SendError {}

impl Drop for Peer {
	fn drop(&mut self) {
		unsafe { raknet_RakPeerInterface_DestroyInstance(self.ptr) };
	}
}

#[repr(u8)]
#[derive(Debug, PartialEq, num_derive::FromPrimitive)]
pub enum StartupResult {
	RaknetStarted,
	RaknetAlreadyStarted,
	InvalidSocketDescriptors,
	InvalidMaxConnections,
	SocketFamilyNotSupported,
	SocketPortAlreadyInUse,
	SocketFailedToBind,
	SocketFailedTestSend,
	PortCannotBeZero,
	FailedToCreateNetworkThread,
	CouldNotGenerateGuid,
	StartupOtherFailure,
}

impl StartupResult {
	pub fn try_from(u: u32) -> Option<StartupResult> {
		num_traits::FromPrimitive::from_u32(u)
	}
}

#[repr(u8)]
#[derive(Debug, PartialEq, num_derive::FromPrimitive)]
pub enum ConnectionAttemptResult {
	ConnectionAttemptStarted,
	InvalidParameter,
	CannotResolveDomainName,
	AlreadyConnectedToEndpoint,
	ConnectionAttemptAlreadyInProgress,
	SecurityInitializationFailed,
}

impl ConnectionAttemptResult {
	pub fn try_from(u: u32) -> Option<ConnectionAttemptResult> {
		num_traits::FromPrimitive::from_u32(u)
	}
}
