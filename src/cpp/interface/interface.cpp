#include "../original/RakPeerInterface.h"
#include <iostream>
#include <cstdint>

using namespace RakNet;

extern "C"
{
	/** RakPeerInterface **/
	void *raknet_RakPeerInterface_GetInstance()
	{
		return RakPeerInterface::GetInstance();
	}

	void raknet_RakPeerInterface_DestroyInstance(void *peer)
	{
		RakPeerInterface::DestroyInstance(static_cast<RakPeerInterface *>(peer));
	}

	uint32_t raknet_RakPeerInterface_Startup(void *peer, unsigned int maxConnections, void *socketDescriptor)
	{
		auto *peer_ = static_cast<RakPeerInterface *>(peer);
		auto *socketDescriptor_ = static_cast<SocketDescriptor *>(socketDescriptor);
		return peer_->Startup(maxConnections, socketDescriptor_, 1);
	}

	uint32_t raknet_RakPeerInterface_Connect(void *peer, const char *host, unsigned short remotePort)
	{
		auto *peer_ = static_cast<RakPeerInterface *>(peer);
		return static_cast<uint32_t>(peer_->Connect(host, remotePort, nullptr, 0));
	}

	void raknet_RakPeerInterface_SetMaxIncomingConnections(void *peer, unsigned short maxConnections)
	{
		auto *peer_ = static_cast<RakPeerInterface *>(peer);
		peer_->SetMaximumIncomingConnections(maxConnections);
	}

	void *raknet_RakPeerInterface_Receive(void *peer)
	{
		auto *peer_ = static_cast<RakPeerInterface *>(peer);
		return peer_->Receive();
	}

	void raknet_RakPeerInterface_DeallocatePacket(void *peer, void *packet)
	{
		auto *peer_ = static_cast<RakPeerInterface *>(peer);
		auto *packet_ = static_cast<Packet *>(packet);
		peer_->DeallocatePacket(packet_);
	}

	uint32_t raknet_RakPeerInterface_Send(void *peer, void *data, int length, void *address)
	{
		auto *peer_ = static_cast<RakPeerInterface *>(peer);
		auto *data_ = static_cast<const char *>(data);
		auto *address_ = static_cast<AddressOrGUID *>(address);
		return peer_->Send(data_, length, PacketPriority::MEDIUM_PRIORITY, PacketReliability::RELIABLE, 0, *address_, false);
	}

	/** SocketDescriptor **/
	void *raknet_SocketDescriptor_Constructor(unsigned short port, char *hostAddress)
	{
		SocketDescriptor *socketDescriptor = new SocketDescriptor(port, hostAddress);
		return socketDescriptor;
	}

	void raknet_SocketDescriptor_Destructor(void *socketDescriptor)
	{
		SocketDescriptor *socketDescriptor_ = static_cast<SocketDescriptor *>(socketDescriptor);
		delete socketDescriptor_;
	}

	/** Packet **/
	unsigned char *raknet_Packet_Data(void *packet)
	{
		auto *packet_ = static_cast<Packet *>(packet);
		return packet_->data;
	}

	unsigned int raknet_Packet_Length(void *packet)
	{
		auto *packet_ = static_cast<Packet *>(packet);
		return packet_->length;
	}

	void *raknet_Packet_Address(void *packet)
	{
		auto *packet_ = static_cast<Packet *>(packet);
		return new AddressOrGUID{packet_->systemAddress};
	}

	/** AddressOrGUID **/
	void *raknet_AddressOrGUID(char *address, unsigned short port)
	{
		return new AddressOrGUID{SystemAddress{address, port}};
	}

	bool raknet_AddressOrGUID_Equality(void *a, void *b)
	{
		auto *address_a = static_cast<AddressOrGUID *>(a);
		auto *address_b = static_cast<AddressOrGUID *>(b);
		return *address_a == *address_b;
	}

	//static unsigned long ToInteger( const AddressOrGUID &aog );
	uint32_t raknet_AddressOrGUID_Hash(void *address)
	{
		auto *a = static_cast<AddressOrGUID *>(address);
		return AddressOrGUID::ToInteger(*a);
	}
}